#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <cblas.h>

void dcopy_(const int *n, const double *x, const int *incx, 
            const double *y, const int *incy);

/* Simulation parameters data structure */ 
typedef struct parameters {
  float T;		/* Total simulation time */
  float dt;		/* Euler's timestep */
  float tau;		/* Synapses decay time constant */
  float alpha;		/* convergence parameter */
}params;

/* Read from a file a two-dimensional matrix and store it to a 
 * one-dimensional vector. 
 */
int read_from_file(char *filename, double **matrix, int row, int col)
{
  int i, j, rfs;
  FILE *fp;

  if (!(fp = fopen( filename, "rb"))) {
    printf("Error! The file %s does not exist.\n", filename);
    return 1;
  }
	
  // for(i = 0; i < row; i++ ){
  //   for(j = 0; j < col; j++ ){
  //     rfs = fscanf(fp, "%lf", &(*matrix)[i*col+j]);
  //   }
  // }
  fread(*matrix, sizeof(double), row * col, fp);

  fclose(fp);
  return 0;
}

/* Write a one-dimensional array to a file as a two-dimensional 
 * matrix
 */
int write_2_file(char *filename, double *vector, int n)
{
  int i, j, count=0;
  FILE *fq;

  if (!(fq = fopen(filename, "w"))){
    printf("Error! Could not write to file %s.\n", filename);
    return 1;
  }

  for( i = 0; i < n; i++ ){
    for( j = 0; j < n; j++ ){
      fprintf(fq, "%f ", vector[i*n+j] );
      count++;
    }
    fprintf(fq,"\n");
  }
  fclose(fq);
  return 0;
}

/* Copies the source b to the destination a, then checks if 
 * elements of a are below threshold. If yes it replaces the 
 * elements by threshold. 
 */
void maximum(double **a, double *b, double th, int dim)
{
  int i;
  int incb=1, inca=1;

  /* memcpy( (*a), b, dim*sizeof( double ) ); */
  cblas_dcopy(dim, b, incb, (*a), inca); 

  for(i = 0; i < dim; i++){
    if(b[i] <= th){
      (*a)[i] = th;
    }
  }
}

/* Assigns the initial conditions to a one-dimensional vector */
void initial_conditions(double **a, double initValue, int row, int col)
{
  int i, j;
  float tmp;

  for(i = 0; i < row; i++){
    for(j = 0; j < col; j++){
      tmp = rand() * 1.0/(((float)(RAND_MAX) + 1.0));
      (*a)[i*col+j] = initValue * tmp;
    }
  }
}

/* Main neural field computations 
 * exc_weights is the excitatory part of the kernel
 * inh_weights is the inhibitory part of the kernel
 * dim is the number of neurons in one dimension (e.g. 32)
 */
void neural_field(params prms, double *exc_weights,
                  double *inh_weights, int dim)
{
  int i, j, steps=(int)(prms.T * 1.0/prms.dt);	/* Steps of the neural field simulation */
  int order = 101, trans = 111;			/* Blas dgemv parameters */
  int row=dim*dim, col=dim*dim;
  int ldw=row, incv=1, incc=1;
  double one=1.0, zero=0.0;			/* Blas dgemv parameters */
  double *u, *v;					/* Neural field activity vectors */
  double *c_exc, *c_inh;				/* Lateral connections vectors */
  double *inp; 					/* Input vector */

  /* Memory allocation */
  u = (double *)calloc(dim*dim, sizeof(double));
  v = (double *)calloc(dim*dim, sizeof(double));
  c_exc = (double *)calloc(dim*dim, sizeof( double));
  c_inh = (double *)calloc(dim*dim, sizeof( double));
  inp = (double *)calloc(dim*dim, sizeof( double));

  /* Assignment of initial values */
  initial_conditions(&u, 0.01, dim, dim);
  initial_conditions(&v, 0.01, dim, dim);

  /* Read the stimulus from a file */
  read_from_file("input.dat", &inp, dim, dim);
  printf("Initialization completed!\n");

  /* Perform the forward Euler's method */
  for( i = 0; i < steps; i++ ){
    /* dot produc of activity with lateral connections */
    cblas_dgemv(order, trans, row, col, one, exc_weights, ldw, v, incv, zero, c_exc, incc ); 
    cblas_dgemv(order, trans, row, col, one, inh_weights, ldw, v, incv, zero, c_inh, incc ); 
    for( j = 0; j < col; j++ ){
      u[j] += (-u[j] + (c_exc[j] - c_inh[j]) + inp[j]) / prms.tau * prms.dt;
    }
    maximum(&v, u, 0.0, col);
  }

  printf("Computations completed!\n");

  /* Writing results to a file */
  write_2_file("c_activity.dat", v, dim);
  printf("Storing data completed!\n");

  /* Memory deallocation */
  free(u);
  free(c_exc);
  free(c_inh);
  free(v);
  free(inp);
}

/* Main function */
int main(int argc, char **argv)
{
  int n = 128;		/* Neural field size */	
  double *exc, *inh; 
  params prms;

  /* Memory allocation for excitatory and inhibitory kernels */
  exc = (double *)calloc(n*n*n*n, sizeof(double *));
  inh = (double *)calloc(n*n*n*n, sizeof(double *));

  /* Read from file the kernels. Kernels have been created by DANA */
  read_from_file("kernel_exc.dat", &exc, n*n, n*n);
  read_from_file("kernel_inh.dat", &inh, n*n, n*n);

  /* Simulation parameters */
  prms.T = 100.0;
  prms.dt = 15.0*0.001;
  prms.tau = 1.0;
  prms.alpha = 0.1;

  neural_field(prms, exc, inh, n);

  /* Memory deallocation */
  free(exc);
  free(inh);

  return 0;
}
