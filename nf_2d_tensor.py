import numpy as np
from dana import *
from time import time
import matplotlib.pylab as plt


def fromdistance(fn, shape, center=(.5, .5), dtype=float):
    def distance(*args):
        d = 0
        for i in range(len(shape)):
            D = args[i] / float(max(1, shape[i])) - center[i]
            d += np.minimum(1 - np.abs(D), np.abs(D))**2
        return np.sqrt(d) / np.sqrt(len(shape))
    return fn(np.fromfunction(distance, shape, dtype=dtype))


def Gaussian(shape, center, sigma=0.5):
    def g(x):
        return np.exp(-(x**2 / (2.0 * sigma**2)))
    return fromdistance(g, shape, center)


def plot_activity(inp_data, data):
    plt.figure(figsize=(9, 3))
    plt.subplot(1, 2, 1)
    plt.imshow(inp_data, interpolation='nearest', cmap=plt.cm.hot)
    plt.colorbar()
    plt.xticks([])
    plt.yticks([])

    plt.subplot(1, 2, 2)
    plt.imshow(data, interpolation='nearest', cmap=plt.cm.hot)
    plt.colorbar()
    plt.xticks([])
    plt.yticks([])


def g(x, sigma=1.0):
    return np.exp(-0.5 * (x / sigma)**2)


def build_kernels(n, flag):
    if flag == 'toric':
        print('Toric DANA Kernel')
        Z = np.random.random((n,n))
        # Z = np.ones((n, n))
        x_inf, x_sup, y_inf, y_sup = 0.0, 1.0, 0.0, 1.0
        X, Y = np.meshgrid(np.linspace(x_inf, x_sup, n+1)[1:],
                           np.linspace(y_inf, y_sup, n+1)[1:])
        D = np.sqrt((X - 0.5)**2 + (Y - 0.5)**2)
        Ke = 3.65 * g(D, 0.1) * 960.0 / (n * n)
        Ki = 2.40 * g(D, 1.0) * 960.0 / (n * n)
        C = DenseConnection(Z, Z, Ke, toric=True).weights
        D = DenseConnection(Z, Z, Ki, toric=True).weights
    else:
        print('Nontoric DANA Kernel')
        p = 2 * n + 1
        # Z = np.random.random((n, n))
        Z = np.ones((n, n))
        Ke = 3.65 * 960.0 / (n * n) * gaussian((p, p), (0.1, 0.1))
        Ki = 2.40 * 960.0 / (n * n) * gaussian((p, p), (1.0, 1.0))
        C = DenseConnection(Z, Z, Ke).weights
        D = DenseConnection(Z, Z, Ki).weights
    return C, D


def pack(data, typ='i'):
    import struct
    data = np.array(data).flatten()
    s = struct.pack(typ*data.shape[0], *data)
    return s


def neural_field(n, T, dt, tau, alpha):
    U = np.random.random((n * n,)) * .0

    We, Wi = build_kernels(n, 'toric')
    W = We - Wi
    print((We-Wi).max())
    with open('kernel_exc.dat', 'wb') as f:
        f.write(pack(We, 'd'))

    with open('kernel_inh.dat', 'wb') as f:
        f.write(pack(We, 'd'))

    with open('kernel.dat', 'wb') as f:
        f.write(pack(W, 'd'))

    # I = 1.0 * Gaussian((n, n), (.5, .5), .05).ravel() * alpha
    I = np.ones((n* n,)) *3* alpha
    with file('input.dat', 'wb') as f:
        f.write(pack(I, 'd'))

    sim_time = int(T / dt)
    t0 = time()
    V_ = []
    for _ in range(sim_time):
        Z = np.maximum(U, 0) * alpha
        Le = np.dot(We, Z)
        Li = np.dot(Wi, Z)
        U += (-U + (Le - Li) + I) * dt / tau
        # L = np.dot(W, Z)
        # U += (-U + L + I) * dt / tau
        V_.append(U.max())
        
    t1 = time()
    print(t1 - t0)
    plt.plot(V_)
    plt.show()

    return I, U


if __name__ == '__main__':
    n = 32
    ms = 0.001
    T = 120.0
    #T = 5.0 * ms
    dt = 5.0 * ms
    tau = 1.0
    alpha = 0.1
    
    I, V = neural_field(n, T, dt, tau, alpha)
    
    plt.figure()
    V = np.maximum(V, 0).reshape(n, n)
    plt.imshow(V.reshape(n,n), interpolation='bicubic', cmap=plt.cm.hot)
    plt.colorbar()
    plt.show()
