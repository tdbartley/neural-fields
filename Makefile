.DEFAULT_GOAL := all

CC = nvcc
CCFLAGS = -O3 --Wno-deprecated-gpu-targets --compiler-options -Wall
TARGETS = nf.o
SRCS_COMMON = timer.c 

all: $(TARGETS)

nf.o: nf.cu $(SRCS_COMMON)
	$(CC) $(CCFLAGS) -o $@ nf.cu $(SRCS_COMMON)

test: nf.o
	./nf.o
	python plot.py &

clean:
	rm -f $(TARGETS) 

# eof
