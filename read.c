#include <stdio.h>
#include <stdlib.h>
#include <error.h>


void read_from_bin(char *fname, double **x, size_t n) {
  FILE *fp;
  
  if(!(fp = fopen(fname, "rb"))) {
    printf("File %s not found!\n", fname);
    exit(-1);
  }
  
  fread(*x, sizeof(double), n, fp);
  
  fclose(fp);
}


int main() {
  int n = 1048576;
  double *x = (double *)malloc(n * sizeof(double));
  read_from_bin("kernel.dat", &x, n);
  
  for (int i = 0; i < n; ++i) {
    printf("%0lf \n", x[i]);
  }
  
  return 0;
}
