// 1D and 2D neural fields simulator
// requires installed cuda and built cuda-samples

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <float.h>
#include "timer.h"

typedef double dtype;

// generate or load from file
//#define LOAD_STATE 1
//#define LOAD_KERNEL 1
//#define LOAD_INPUT 1
// units per layer
#define N 32
// kernel parameters
//#define TORIC 1
//#define SIGMA_E (0.125 * N)
//#define SIGMA_I (0.78 * N)
#define SIGMA_E (0.1 * N)
#define SIGMA_I (0.78 * N)
#define GAIN_E (1.5 * 960)
#define GAIN_I (1.2 * 960)
// time step
#define DT 0.01
// end time
#define T_END 100
// decay rate
#define TAU 1.0
// scale factor for u
#define ALPHA 0.1
// device-specific block limitation
#define MAX_BLOCKS 2147483647
// device-specific thread limitation
#define MAX_THREADS 1024
// total network size
#define NET_SIZE (N * N)


//return the next power of 2 that is greater than or equal to x
unsigned int next_pow2(unsigned int x) {
  --x;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return ++x;
}


// load data from file
void load_bin(char *fname, dtype **x, unsigned int n) {
  FILE *fp;
  
  if (!(fp = fopen(fname, "rb"))) {
    printf("File %s not found!\n", fname);
    exit(-1);
  }
  fread(*x, sizeof(dtype), n, fp);
  fclose(fp);
}


// save data to file
void save_bin(char *fname, dtype **x, unsigned int n) {
  FILE *fp;
  
  if (!(fp = fopen(fname, "wb"))) {
    printf("File %s not found!\n", fname);
    exit(-1);
  }
  fwrite(*x, sizeof(dtype), n, fp);
  fclose(fp);
}


// check device max capacity
void check_max_blocks_threads() {
  cudaDeviceProp prop;
  cudaGetDeviceProperties(&prop, 0);
  unsigned int max_threads = prop.maxThreadsPerBlock;
  unsigned int max_blocks = prop.maxGridSize[0];
  
  if (max_blocks != MAX_BLOCKS) {
    printf("Warning: MAX_BLOCKS set to %d, but device capacity is %d.\n", MAX_BLOCKS, max_blocks);
  }
  if (max_threads != MAX_THREADS) {
    printf("Warning: MAX_THREADS set to %d, but device capacity is %d.\n", MAX_THREADS, max_threads);
  }
}


// generate random array
void random_array(dtype **a, unsigned int n, dtype val) {
  for (unsigned int i = 0; i < n; ++i) {
    (*a)[i] = rand() * val / (((dtype)(RAND_MAX) + 1.0));
  }
}


// generate constant array
void constant_array(dtype **a, unsigned int n, dtype val) {
  for (unsigned int i = 0; i < n; ++i) {
    (*a)[i] = val;
  }
}


// generate gaussian array
void gaussian(dtype **k_base, unsigned int n, dtype sigma,
              dtype gain, unsigned int x_shift, unsigned int y_shift) {
  for (unsigned int i = 0; i < n; ++i) {
    for (unsigned int j = 0; j < n; ++j) {
      unsigned int index = i*n + j;
      int x = i - x_shift;
      int y = j - y_shift;
      dtype r = sqrt(x*x + y*y);
      (*k_base)[index] = gain * (exp(-0.5*(r*r)/(sigma*sigma))) / (n*n);
    }
  }
}


// initialize kernel
void initialize_kernel(dtype **k_e, dtype **k_i, unsigned int n) {
  dtype *g1 = (dtype *)malloc(NET_SIZE * sizeof(dtype));
  dtype *g2 = (dtype *)malloc(NET_SIZE * sizeof(dtype));
  
#ifdef TORIC
  gaussian(&g1, n, SIGMA_E, GAIN_E, n/2, n/2);
  gaussian(&g2, n, SIGMA_I, GAIN_I, n/2, n/2);
  
  // shift and copy to each unit
  for (unsigned int a = 0; a < n; ++a) {
    for (unsigned int b = 0; b < n; ++b) {
      for (unsigned int i = 0; i < n; ++i) {
        for (unsigned int j = 0; j < n; ++j) {
          unsigned int index = (a*n + b)*n*n + i*n + j;
          unsigned int shift_index = ((i+3*n/2-a)%n)*n + (j+3*n/2-b)%n;
          (*k_e)[index] = g1[shift_index];
          (*k_i)[index] = -g2[shift_index];
        }
      }
    }
  }
#else
  
  // generate gaussian centered at each unit
  for (unsigned int a = 0; a < n; ++a) {
    for (unsigned int b = 0; b < n; ++b) {
      gaussian(&g1, n, SIGMA_E, GAIN_E, a, b);
      gaussian(&g2, n, SIGMA_I, GAIN_I, a, b);
      
      for (unsigned int i = 0; i < n; ++i) {
        for (unsigned int j = 0; j < n; ++j) {
          unsigned int index = (a*n + b)*n*n + i*n + j;
          (*k_e)[index] = g1[i*n + j];
          (*k_i)[index] = -g2[i*n + j];
        }
      }
    }
  }
#endif
  
  free(g1);
  free(g2);
}


// find out # of threads and # thread blocks
void get_num_blocks_threads(unsigned int kernel_id,
                            unsigned int n,
                            dim3 &blocks,
                            dim3 &threads) {
  if (kernel_id == 0) { // rectified linear, multiply
    // j index dimension
    threads.x = n < MAX_THREADS ? next_pow2(n) : MAX_THREADS;
    // overflow j index dimension (used when net_size > MAX_THREADS)
    blocks.x = (n + (threads.x - 1))/(threads.x);
    // i index dimension
    blocks.y = n;
  } else if (kernel_id == 1) { // summation
    // j index dimension
    threads.x = (n < 2 * MAX_THREADS) ? next_pow2((n + 1)/2) : MAX_THREADS;
    // overflow j index dimension (used when net_size > MAX_THREADS)
    blocks.x = (n + 2 * threads.x - 1)/(2 * threads.x);
    // i index dimension
    blocks.y = NET_SIZE;
  } else if (kernel_id == 2) { // get max value
    // i index dimension
    threads.x = (n < 2 * MAX_THREADS) ? next_pow2((n + 1)/2) : MAX_THREADS;
    // overflow i index dimension (used when net_size > MAX_THREADS)
    blocks.x = (n + 2 * threads.x - 1)/(2 * threads.x);
    // unused
    blocks.y = 1;
  } else { // closing arithmetic and write output
    // i index dimension
    threads.x = n < MAX_THREADS ? next_pow2(n) : MAX_THREADS;
    // overflow i index dimension (used when net_size > MAX_THREADS)
    blocks.x = (n + (threads.x - 1))/(threads.x);
    // unused
    blocks.y = 1;
  }
  
  // unused dimensions
  threads.y = 1;
  threads.z = 1;
  blocks.z = 1;
}


// neural field kernel 0
// rectified linear, multiply
__global__ void nf0(dtype *u, dtype *k_e, dtype *k_i, dtype *f) {
  unsigned int i = blockIdx.y;
  unsigned int j = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int addr = NET_SIZE * i + j;
  
  if (j < NET_SIZE) {
    f[addr] = u[j] < 0 ? 0 : (k_e[addr]+k_i[addr]) * u[j] * ALPHA;
  }
}


// neural field kernel 1
// summation
__global__ void nf1(dtype *g_idata, dtype *g_odata, unsigned int n) {
  __shared__ dtype scratch[MAX_THREADS];
  unsigned int i = blockIdx.y;
  unsigned int j = blockIdx.x * blockDim.x + threadIdx.x;
  unsigned int addr = NET_SIZE * i + j;
  
  // add pairs of inputs when loading
  if (j < n/2) {
    scratch[threadIdx.x] = g_idata[addr] + g_idata[addr + n/2];
  } else {
    scratch[threadIdx.x] = 0;
  }
  __syncthreads ();
  
  // sum all remaining elements
  for (unsigned int s = blockDim.x/2; s > 0; s = s/2) {
    if (threadIdx.x < s) {
      scratch[threadIdx.x] += scratch[threadIdx.x + s];
    }
    __syncthreads ();
  }
  
  // thread 0 saves final result to memory
  if (threadIdx.x == 0) {
    g_odata[NET_SIZE * i + blockIdx.x] = scratch[0];
  }
}


// neural field kernel 2
// get max value
__global__ void nf2(dtype *g_idata, dtype *max_val, unsigned int n, unsigned int t) {
  __shared__ dtype scratch[MAX_THREADS];
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  
  // compare pairs of inputs when loading
  if (i < n/2) {
    scratch[threadIdx.x] = g_idata[i*NET_SIZE] > g_idata[(i+n/2)*NET_SIZE]? g_idata[i*NET_SIZE] : g_idata[(i+n/2)*NET_SIZE];
  } else {
    scratch[threadIdx.x] = -DBL_MAX;
  }
  __syncthreads ();
  
  // compare all remaining elements
  for (unsigned int s = blockDim.x/2; s > 0; s = s/2) {
    if (threadIdx.x < s) {
      scratch[threadIdx.x] = scratch[threadIdx.x] > scratch[threadIdx.x + s] ? scratch[threadIdx.x] : scratch[threadIdx.x + s];
    }
    __syncthreads ();
  }
  
  // thread 0 saves final result to memory
  if (threadIdx.x == 0) {
    max_val[t] = scratch[0];
  }
}


// neural field kernel 3
// closing arithmetic and write output
__global__ void nf3(dtype *u, dtype *src, dtype *sum, dtype tau, dtype dt) {
  unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
  
  if (i < NET_SIZE) {
    u[i] += dt / tau * (-u[i] + sum[NET_SIZE * i] + src[i]); 
  }
}


// run simulation
int main(int argc, char** argv) {
  check_max_blocks_threads();
  
  // device buffers
  dtype *u_dev=NULL;
  dtype *u_max_dev=NULL;
  dtype *sum_dev=NULL;
  dtype *k_e_dev=NULL;
  dtype *k_i_dev=NULL;
  dtype *src_dev=NULL;
  
  // initialize network state
  dtype *u = (dtype *)malloc(NET_SIZE * sizeof(dtype));
#ifdef LOAD_STATE  
  load_bin((char *)"input_state.dat", &u, NET_SIZE);
#else
  constant_array(&u, NET_SIZE, 1.0);
#endif
  cudaMalloc((void **)&u_dev, NET_SIZE * sizeof(dtype));
  cudaMemcpy(u_dev, u, NET_SIZE * sizeof(dtype), cudaMemcpyHostToDevice);
  save_bin((char *)"state.dat", &u, NET_SIZE);
  free(u);
  
  // initialize kernel
  dtype *k_e = (dtype *)malloc(NET_SIZE * NET_SIZE * sizeof(dtype));
  dtype *k_i = (dtype *)malloc(NET_SIZE * NET_SIZE * sizeof(dtype));
#ifdef LOAD_KERNEL
  load_bin((char *)"input_kernel_e.dat", &k_e, NET_SIZE * NET_SIZE);
  load_bin((char *)"input_kernel_i.dat", &k_i, NET_SIZE * NET_SIZE);
#else
  initialize_kernel(&k_e, &k_i, N);
#endif
  cudaMalloc((void **)&k_e_dev, NET_SIZE * NET_SIZE * sizeof(dtype));
  cudaMalloc((void **)&k_i_dev, NET_SIZE * NET_SIZE * sizeof(dtype));
  cudaMemcpy(k_e_dev, k_e, NET_SIZE * NET_SIZE * sizeof(dtype), cudaMemcpyHostToDevice);
  cudaMemcpy(k_i_dev, k_i, NET_SIZE * NET_SIZE * sizeof(dtype), cudaMemcpyHostToDevice);
  save_bin((char *)"kernel_e.dat", &k_e, NET_SIZE * NET_SIZE);
  save_bin((char *)"kernel_i.dat", &k_i, NET_SIZE * NET_SIZE);
  free(k_e);
  free(k_i);
  
  // initialize input data
  dtype *src = (dtype *)malloc(NET_SIZE * sizeof(dtype));
#ifdef LOAD_INPUT
  load_bin((char *)"input_stimulus.dat", &src, NET_SIZE);
#else
  random_array(&src, NET_SIZE, 1.0);
#endif
  cudaMalloc((void **)&src_dev, NET_SIZE * sizeof(dtype));
  cudaMemcpy(src_dev, src, NET_SIZE * sizeof(dtype), cudaMemcpyHostToDevice);
  save_bin((char *)"stimulus.dat", &src, NET_SIZE);
  free(src);
  
  // launch gpu kernels
  cudaMalloc((void **)&sum_dev, NET_SIZE * NET_SIZE * sizeof(dtype));
  cudaMalloc((void **)&u_max_dev, T_END / DT * sizeof(dtype));
  struct stopwatch_t* timer = NULL;
  stopwatch_init();
  timer = stopwatch_create();
  stopwatch_start(timer);
  for (unsigned int t = 0; t < T_END / DT; ++t) {
    dim3 blocks;
    dim3 threads;
    
    // rectified linear, multiply
    get_num_blocks_threads(0, NET_SIZE, blocks, threads);
    nf0<<<blocks, threads>>>(u_dev, k_e_dev, k_i_dev, sum_dev);
    cudaThreadSynchronize();
    
    // summation
    get_num_blocks_threads(1, NET_SIZE, blocks, threads);
    nf1<<<blocks, threads>>>(sum_dev, sum_dev, NET_SIZE);
    unsigned int s = blocks.x;
    while (s > 1) {
      get_num_blocks_threads(1, s, blocks, threads);
      nf1<<<blocks, threads>>> (sum_dev, sum_dev, s);
      s = (s + threads.x * 2 - 1) / (threads.x * 2);
    }
    cudaThreadSynchronize();
    
    // find max value
    get_num_blocks_threads(2, NET_SIZE, blocks, threads);
    nf2<<<blocks, threads>>>(sum_dev, u_max_dev, NET_SIZE, t);
    s = blocks.x;
    while (s > 1) {
      get_num_blocks_threads(1, s, blocks, threads);
      nf2<<<blocks, threads>>> (sum_dev, u_max_dev, s, t);
      s = (s + threads.x * 2 - 1) / (threads.x * 2);
    }
    cudaThreadSynchronize();
    
    // closing arithmetic and write output
    get_num_blocks_threads(3, NET_SIZE, blocks, threads);
    nf3<<<blocks, threads>>>(u_dev , src_dev, sum_dev, TAU, DT);
    cudaThreadSynchronize();
  }
  
  // save state to file
  u = (dtype *)malloc(NET_SIZE * sizeof(dtype));
  cudaMemcpy(u, u_dev, NET_SIZE * sizeof(dtype), cudaMemcpyDeviceToHost);
  save_bin((char *)"output.dat", &u, NET_SIZE);
  free(u);
  
  // save state to file
  dtype *u_max = (dtype *)malloc(T_END / DT * sizeof(dtype));
  cudaMemcpy(u_max, u_max_dev, T_END / DT * sizeof(dtype), cudaMemcpyDeviceToHost);
  save_bin((char *)"u_max.dat", &u_max, T_END / DT);
  // for (unsigned int i = 0; i < T_END / DT; ++i) {
  //   printf("u_max[%d] %lf\n", i, (u_max)[i]);
  // }
  free(u_max);
  
  // print execution time
  long double t_kernel = stopwatch_stop(timer);
  printf("GPU execution time: %Lg s.\n", t_kernel);
  
  // free device memory
  cudaFree(u_dev);
  cudaFree(u_max_dev);
  cudaFree(src_dev);
  cudaFree(k_e_dev);
  cudaFree(k_i_dev);
  cudaFree(sum_dev);
  
  return 0;
}
