import sys
import numpy as np
from time import clock
import matplotlib.pylab as plt
from numpy.fft import rfft2, irfft2


def best_fft_shape(shape):
    # fftpack (not sure of the base)
    base = [13, 11, 7, 5, 3, 2]
    # fftw
    # base = [13,11,7,5,3,2]

    def factorize(n):
        if n == 0:
            raise(RuntimeError, "Length n must be positive integer")
        elif n == 1:
            return [1, ]
        factors = []
        for b in base:
            while n % b == 0:
                n /= b
                factors.append(b)
        if n == 1:
            return factors
        return []

    def is_optimal(n):
        factors = factorize(n)
        # fftpack
        return len(factors) > 0

    shape = np.atleast_1d(np.array(shape))
    for i in range(shape.size):
        while not is_optimal(shape[i]):
            shape[i] += 1
    return shape.astype(int)


def fromdistance(fn, shape, center=(.5, .5), dtype=float):
    def distance(*args):
        d = 0
        for i in range(len(shape)):
            D = args[i] / float(max(1, shape[i])) - center[i]
            d += np.minimum(1 - np.abs(D), np.abs(D))**2
        return np.sqrt(d) / np.sqrt(len(shape))
    return fn(np.fromfunction(distance, shape, dtype=dtype))


def Gaussian(shape, center, sigma=0.5):
    def g(x): return 1.0 * np.exp(-x**2 / sigma**2)
    return fromdistance(g, shape, center)


def gaussian(shape=(25, 25), width=(1, 1), center=(0, 0)):
    ''' Kind of Gaussian '''
    grid = []
    for size in shape:
        grid.append(slice(0, size))
    C = np.mgrid[tuple(grid)]
    R = np.zeros(shape)
    for i, size in enumerate(shape):
        if shape[i] > 1:
            R += (((C[i]/float(size-1))*2 - 1 - center[i])/width[i])**2
    return np.exp(-R/2)


if __name__ == '__main__':
    n = 32
    p = 2*n+1
    T = 100.0
    ms = 0.001
    dt = 10.0 * ms
    tau = 1.0
    alpha = 0.1
    sim_time = int(T / dt)

    if len(sys.argv) > 1 and sys.argv[1] == 'gauss':
        I = 0.9 * Gaussian((n, n), (0.5, 0.5), 0.1) * alpha
    else:
        I = 2.0 * alpha * np.ones((n, n))

    U = np.random.random((n, n)) * .0

    scale = 960./(n*n)
    We = 1.50 * gaussian((p, p), (0.1, 0.1)) * scale
    Wi = 0.75 * gaussian((p, p), (1.0, 1.0)) * scale

    We_shape, U_shape = np.array(We.shape), np.array(U.shape)
    shape = np.array(best_fft_shape(U_shape + We_shape//2))

    We_fft = rfft2(We, shape)
    Wi_fft = rfft2(Wi, shape)

    i0 = We.shape[0] // 2
    i1 = i0 + U_shape[0]
    j0 = We.shape[1] // 2
    j1 = i0 + U_shape[1]

    t0 = clock()
    for _ in range(sim_time):
        Z = rfft2(alpha*np.maximum(U, 0), shape)
        Le = irfft2(Z * We_fft, shape).real[i0:i1, j0:j1]
        Li = irfft2(Z * Wi_fft, shape).real[i0:i1, j0:j1]
        U += (-U + (Le - Li) + I) * dt / tau
    t1 = clock()
    print(t1 - t0)

    plt.figure()
    plt.imshow(np.maximum(U, 0), interpolation='nearest', cmap=plt.cm.hot)
    plt.colorbar()
    plt.show()
