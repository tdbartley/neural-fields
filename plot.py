import numpy as np
import matplotlib.pylab as plt
import struct

if __name__ == '__main__':
    n = 32
    t = 10000
    
    fig, axarr = plt.subplots(2, 3)
    fig.set_size_inches(16, 9, forward=True)
    fig.subplots_adjust(left=0.02, bottom=0.06, right=0.95, top=0.94, wspace=0.05)
    
    # e weights
    with open('kernel_e.dat', 'rb') as f:
        c = f.read()
    k_e = np.array(struct.unpack('d'*n*n*n*n, c)).reshape(n*n, n*n)
   
    # i weights
    with open('kernel_i.dat', 'rb') as f:
        c = f.read()
    k_i = np.array(struct.unpack('d'*n*n*n*n, c)).reshape(n*n, n*n)
    
    # unit 0 weights
    im = axarr[0, 0].imshow((k_e[0]+k_i[0]).reshape(n, n), interpolation='nearest', cmap=plt.cm.inferno)
    axarr[0, 0].set_title('Unit (0, 0) weights')
    fig.colorbar(im, ax=axarr[0, 0])
    
    # top left neuron weights
    im = axarr[0, 1].plot((k_e[0]+k_i[0]).reshape(n, n), 'b')
    axarr[0, 1].set_title('Unit (0, 0) weights')
    axarr[0, 1].set_xlim([0,n-1])
    
    # u_max
    with open('u_max.dat', 'rb') as f:
        c = f.read()
    x = np.array(struct.unpack('d'*t, c))
    im = axarr[0, 2].plot(x, 'b')
    axarr[0, 2].set_title('Max value')
    
    # initial state
    with open('state.dat', 'rb') as f:
        c = f.read()
    x = np.array(struct.unpack('d'*n*n, c)).reshape(n, n)
    im = axarr[1, 0].imshow(x, interpolation='nearest', cmap=plt.cm.inferno)
    axarr[1, 0].set_title('Initial state')
    fig.colorbar(im, ax=axarr[1, 0])
    
    # stimulus
    with open('stimulus.dat', 'rb') as f:
        c = f.read()
    x = np.array(struct.unpack('d'*n*n, c)).reshape(n, n)
    im = axarr[1, 1].imshow(x, interpolation='nearest', cmap=plt.cm.inferno)
    axarr[1, 1].set_title('Stimulus')
    fig.colorbar(im, ax=axarr[1, 1])
    
    # final output
    with open('output.dat', 'rb') as f:
        c = f.read()
    x = np.array(struct.unpack('d'*n*n, c)).reshape(n, n)
    im = axarr[1, 2].imshow(np.maximum(x, 0), interpolation='nearest', cmap=plt.cm.inferno)
    axarr[1, 2].set_title('Output')
    fig.colorbar(im, ax=axarr[1, 2])
    
    plt.show()
    
